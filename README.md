# Escornabot-EXTpad

> [**Escornabot EXT**](https://ext.escornabot.org/) (codename *"Kanoi"*) is an evolution of the [Escornabot](https://github.com/escornabot) project, a free and open source software and hardware project originally created by [Tucho Méndez](https://github.com/procastino), [Rafa Couto](https://github.com/rafacouto) and [me](https://gitlab.com/xoan), that aims to bring robotics and programming to children.
> 
> The purpose of this evolution is to make it more flexible and versatile.

![](docs/EXTpad.png)

This repository contains all the source code, manufacturing files and assembly documentation for the EXTpad PCB, the *keypad* for Escornabot EXT *"Kanoi"*.

## Documentation

Pinout, and assembly and commissioning documentation can be found in [the wiki](https://gitlab.com/xoan/escornabot-extpad/wikis/home).

## License

Copyright © 2018 Xoán Sampaíño

This documentation describes Open Hardware and is licensed under the
CERN OHL v1.2.

You may redistribute and modify this documentation under the terms of the
CERN OHL v1.2. (http://ohwr.org/cernohl). This documentation is distributed
WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF
MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A
PARTICULAR PURPOSE. Please see the CERN OHL v1.2 for applicable
conditions.
